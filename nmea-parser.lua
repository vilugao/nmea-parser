#!/usr/bin/env lua
-- NMEA reference: <https://gpsd.gitlab.io/gpsd/NMEA.html>.

local function format_time(v)
	local h, m, s = v:match("^(%d%d)(%d%d)(%d%d%.%d+)$")
	if not h then h, m, s = v:match("^(%d%d)(%d%d)(%d%d)$") end
	if not h then return nil end
	return string.format("%s:%s:%s UTC", h, m, s)
end

local function format_latlng(v, dir)
	local d, m = v:match("^(%d-)(%d%d%.%d+)$")
	if not d then return nil end
	return string.format("%d° %.4f' %s", tonumber(d, 10), tonumber(m), dir)
end

local function string_or_nil(s, u)
	return #s > 0 and s .. u or nil
end

local extractfn = {
	GGA = function (fields)
		local fix = {
			[0] = "N/A", "GPS", "DGPS",
			"PPS", "RTK", "Float RTK",
			"Estimated", "Manual input", "Simulation mode",
		}
		return {
			{"Time", format_time(fields[1])},
			{"Latitude", format_latlng(fields[2], fields[3])},
			{"Longitude", format_latlng(fields[4], fields[5])},
			{"Quality", fix[tonumber(fields[6], 10)]},
			{"Used satellites", tonumber(fields[7], 10)},
			{"HDOP", string_or_nil(fields[8], " m")},
			{"Altitude", string_or_nil(fields[9], " " .. fields[10]:lower())},
			{"Geoid", string_or_nil(fields[11], " " .. fields[12]:lower())},
			{"Age of DGPS", tonumber(fields[13])},
			{"DGPS reference", fields[14]},
		}
	end,

	GSA = function (fields)
		return {
			{"Selection mode", ({A = "Automatic", M = "Manual"})[fields[1]]},
			{"Fix", ({"No", "2D", "3D"})[tonumber(fields[2], 10)]},
			{"PDOP", fields[15]},
			{"HDOP", fields[16]},
			{"VDOP", fields[17]},
		}
	end,

	RMC = function (fields)
		local speed = tonumber(fields[7])
		if speed then
			speed = string.format("%.2f kt = %d km/h = %.1f m/s",
				speed, speed * 1.852, speed * 1.852 / 3.6)
		end
		local function format_date(v)
			local d, m, y = v:match("^(%d%d)(%d%d)(%d%d)$")
			if not d then return nil end
			return string.format("%s%s-%s-%s UTC",
				tonumber(y, 10) >= 40 and "19" or "20", y, m, d)
		end
		local faa = {
			A = "Autonomous", D = "Differential", E = "Estimated",
			M = "Manual input", S = "Simulation", N = "Data not valid",
		}
		return {
			{"Time", format_time(fields[1])},
			{"Status", ({A = "Valid", V = "Warning"})[fields[2]]},
			{"Latitude", format_latlng(fields[3], fields[4])},
			{"Longitude", format_latlng(fields[5], fields[6])},
			{"Speed", speed},
			{"Course", string_or_nil(fields[8], "°")},
			{"Date", format_date(fields[9])},
			{"Magnetic var.", string_or_nil(fields[10], "° " .. fields[11])},
			{"FAA mode", faa[fields[12]]},
		}
	end,
}

for line in io.lines(arg[1]) do
	line = line:gsub("\r", "")
	local m, talkerid, msgtype, fields, cksum =
		line:match("^$((%u%u)(%u%u%u),(.+))%*(%x%x)$")
	if m then
		cksum = tonumber(cksum, 16)
		for c in m:gmatch(".") do
			cksum = bit32.bxor(cksum, c:byte())
		end
		if cksum == 0 then
			local ef = extractfn[msgtype]
			if ef then
				local fs = {}
				for f in fields:gmatch(",?([^,]*)") do
					table.insert(fs, f)
				end
				print(string.format("--- %s %s: %s", talkerid, msgtype, fields))
				for _, x in ipairs(ef(fs)) do
					local v = x[2]
					if (type(v) ~= "string" or #v > 0) and v ~= nil then
						print(string.format("  %s: %s.", x[1], v))
					end
				end
				print("---")
			else
				print(line)
			end
		--else
			--print(line .. " # Invalid checksum!")
		end
	--else
		--print(line)
	end
end
